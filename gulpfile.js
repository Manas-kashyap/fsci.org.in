var gulp = require('gulp');
var htmlhint = require('gulp-htmlhint');
var jshint = require('gulp-jshint');
var csslint = require('gulp-csslint');
var pug = require('gulp-pug');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');

var connect = require('gulp-connect');
var connectProxy = require('gulp-connect-proxy');

var yamlinc = require('gulp-yaml-include');
var runSequence = require('run-sequence');
var readYaml = require('read-yaml');
var glob = require('glob');
var yaml = require('gulp-yaml');
var sortJSON = require('gulp-json-sort').default;

var fs = require('fs');


gulp.task('assetscopy', function() {
  gulp.src([
    'node_modules/bootstrap/fonts/*.{ttf,woff,eot,svg, woff2}',
    'node_modules/font-awesome/fonts/*.{ttf,woff,eot,svg,woff2,otf}',
    'fonts/*.{ttf,woff,woff2}',
    'node_modules/bootcards/dist/fonts/*.{ttf,woff,eot,svg,woff2,otf}'
  ]).pipe(gulp.dest('public/fonts'));
  gulp.src([
    'node_modules/leaflet/dist/images/**',
    'img/**'
  ]).pipe(gulp.dest('public/img'));
});

gulp.task('htmllint', function() {
  gulp.src("public/*.html")
    .pipe(htmlhint());
});

gulp.task('csslint', function() {
  gulp.src('css/*.css')
    .pipe(csslint({
      'shorthand': false
    }))
    .pipe(csslint.formatter());
});

gulp.task('jslint', function() {
  gulp.src(["gulpfile.js", "js/*.js"])
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('GenerateIndexPage', function() {
  gulp.src('pug/pages/*.pug')
    .pipe(pug({
      pretty: true,
      data: {
        debug: true
      }
    }))
    .pipe(gulp.dest('public'));
});

gulp.task('GenerateEventsYAML', function() {
  return gulp.src("events/index.yaml")
    .pipe(yamlinc())
    .pipe(gulp.dest('public/events'));
});

gulp.task('GenerateEventsJSON', function() {
  return gulp.src('public/events/index.yaml')
    .pipe(yaml())
    .pipe(sortJSON({
      cmp: function(a, b) {
        return a.key < b.key ? 1 : -1;
      },
      space: 2
    }))
    .pipe(gulp.dest('public/events'))
});

gulp.task('GenerateEventsIndex', function() {
  fs.readFile('./public/events/index.json', "utf-8", function(err, sortedEvents){
    if(err){
      throw err;
    }
    gulp.src('pug/templates/events.pug')
      .pipe(pug({
        pretty: true,
        data: {
          debug: true,
          events: JSON.parse(sortedEvents)
        }
      }))
      .pipe(concat('index' + '.html'))
      .pipe(gulp.dest('public/events'));
  });
});

gulp.task('GenerateEventPages', function() {
  glob("events/details/*.yaml", {}, function(er, files) {
    var nooffiles = files.length;
    for (var i = 0; i < nooffiles; i++) {
      readYaml(files[i], function(err, ydata) {
        if (err) throw err;
        var id = ydata.id;
        gulp.src('pug/templates/individual_event_page.pug')
          .pipe(pug({
            pretty: true,
            data: {
              debug: true,
              evt: ydata
            }
          }))
          .pipe(concat(id + '.html'))
          .pipe(gulp.dest('public/events'));
      });
    }
  });
});

gulp.task('pughtml', function() {
  runSequence(
    ['GenerateIndexPage', 'GenerateEventPages'],
    'GenerateEventsYAML',
    'GenerateEventsJSON',
    'GenerateEventsIndex'
  )
});

gulp.task('cssmin', function() {
  gulp.src([
      'node_modules/bootstrap/dist/css/bootstrap.min.css',
      'node_modules/font-awesome/css/font-awesome.min.css',
      //'node_modules/normalize.css/normalize.css',
      'node_modules/leaflet/dist/leaflet.css',
      'node_modules/bootcards/dist/css/bootcards-desktop.min.css',
      // 'node_modules/bootcards/dist/css/bootcards-android.min.css',
      // 'node_modules/bootcards/dist/css/bootcards-ios.min.css',
      'css/*.css'
    ])
    .pipe(concat('index.css'))
    .pipe(cssmin())
    .pipe(gulp.dest('public/css'));
});

gulp.task('jsuglify', function() {
  gulp.src([
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/bootstrap/dist/js/bootstrap.min.js',
      'node_modules/leaflet/dist/leaflet.js',
      'node_modules/bootcards/dist/js/bootcards.min.js',
      'js/*.js'
    ])
    .pipe(concat('index.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
  gulp.src([
      'node_modules/html5shiv/dist/html5shiv.js',
      'node_modules/Respond.js/src/respond.js'
    ])
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
});

gulp.task('verify', [
  'public',
  'htmllint',
  'csslint',
  'jslint'
]);

gulp.task('public', [
  'assetscopy',
  'pughtml',
  'cssmin',
  'jsuglify'
]);

gulp.task('connect', function() {
  connect.server({
    root: 'public',
    port: 8080,
    livereload: true
  });
});

gulp.task('watch', function() {
  gulp.watch([
    './pug/**/*.pug'
  ], [
    'pughtml',
    'htmllint',
  ]);
  gulp.watch([
    'css/*.css',
    'package.json'
  ], [
    'csslint',
    'cssmin'
  ]);
  gulp.watch([
    'js/*.js',
    'package.json'
  ], [
    'jslint',
    'jsuglify',
  ]);
});

gulp.task('serve', [
  'public',
  'connect',
  'watch'
]);

gulp.task('default', [
  'serve'
]);
